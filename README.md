# Manĝaĵo

A basic food tracker utilizing Python. This is based on my project, Akvo.

## Planned Features

* Nutrient Tracking
    * Macronutrients (This has priority)
    * Micronutrients
        * Vitamins
        * Dietary Minerals (i.e sodium, potassium etc.)
* Saving history in form of CSVs (Give users ability to see past) 
* Food cache to speed up repeated use of the same foods in a CSV
* Ability to search OpenFoodFacts for food data
    * Ability to search using barcodes
* Ability to contribute to OpenFoodFacts from within application


https://world.openfoodfacts.org/
